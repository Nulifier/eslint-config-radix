module.exports = {
	extends: "./index",
	env: {
		node: true,
		jest: true
	},
	rules: {
		"no-alert": ["warn"]
	}
};
