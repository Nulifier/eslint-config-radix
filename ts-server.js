module.exports = {
	extends: "./ts",
	parser: "@typescript-eslint/parser",
	plugins: [
		"@typescript-eslint/eslint-plugin"
	],
	env: {
		node: true,
		jest: true
	},
	rules: {
		"no-alert": ["warn"]
	}
};
