module.exports = {
	extends: "./ts",
	parser: "@typescript-eslint/parser",
	plugins: [
		"@typescript-eslint/eslint-plugin"
	],
	env: {
		browser: true
	},
	rules: {
		"no-alert": ["warn"]
	}
};
